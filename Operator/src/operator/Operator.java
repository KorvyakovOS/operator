
package operator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Operator {

    public static void main(String[] args){
       
        double a;
        double b;
        
        String fname = "input.txt";
        String fname2 = "output.txt";
        File file = new File(fname);
        File file2 = new File(fname2);
        Scanner sc;
        PrintWriter pw;
                
        try{
            sc = new Scanner(file);
        }catch(FileNotFoundException ex){
            System.out.println("File input not found!");
            return;
        }
        
        try{
            pw = new PrintWriter(file2);
        }catch(FileNotFoundException ex){
            System.out.println("File output not found!");
            return;
        }
        
        while(sc.hasNext()){
        String [] arr = sc.nextLine().split(" ");
               
        try{
            a = Double.parseDouble(arr[0]);
            b = Double.parseDouble(arr[2]);
            
                    switch (arr[1]) {
            case "*":
                System.out.println(a * b);
                pw.println(a * b);
                break;
            case "+":
                System.out.println(a + b);
                pw.println(a + b);
                break;
            case "-":
                System.out.println(a - b);
                pw.println(a - b);
                break;
            case "/":
                if(b != 0){
                    System.out.println(a / b);
                    pw.println(a / b);
                }else{
                    System.out.println("Error! Division by zero");
                    pw.println("Error! Division by zero");
                }
                break;
            default:
                System.out.println("Operation Error!");
                pw.println("Operation Error!");
                break;
        }
            
        }catch(NumberFormatException ex){
            System.out.println("Error! Not number");
            pw.println("Error! Not number");
        }
        

        }
        sc.close();
        pw.close();
    }
    
}
